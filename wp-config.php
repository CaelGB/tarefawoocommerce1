<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'GCyKBrC9odb0APGFliO4EV4yX4gLKJlbMe9pUWR4XwzYb5ejXFkjk4rAqCX4zdDx9yeQqTe/BFIRb0KdjJdcug==');
define('SECURE_AUTH_KEY',  'ohjvQha32+QlH85WafXjTK+dR4zxFqcStzDn9vQgrvsgrBEJ0NJiWb8dL5m7juL/AAhIMb0fuehVbk+IxX6NCg==');
define('LOGGED_IN_KEY',    'EzkL7VlXmFixAHKPJ30QbocvC5iNenl7RNKjPfh/8Vxcr2HCgSum7Q+mSXTHrjCTZccZWJMhy6tDtMFjzA2I9g==');
define('NONCE_KEY',        'Szp9HNWeVoO/mnmU+2qUz7LxQAAqp+IuJheOc/mglg42yaIML8VQJhe9xkz77qV4UHWGyF9ZCC4rH0K9VJ91Lg==');
define('AUTH_SALT',        'cny+eqMb8QFB8Bs+ySvRHLGvdN49jLuh8EHlXRQth/e/ooHqbt3TSQZd/Xhkza3d81faI6i/RCD6dp1RRUOkHQ==');
define('SECURE_AUTH_SALT', 'EcpnRV32Ml7G/tbJJc2s+XWMorWSjXlIm5VFMyZuABQ0kV8W6ZNHCrlK9PqAwkdGuAqoXv6IVqpyGpc/zkEUvg==');
define('LOGGED_IN_SALT',   '/lFYDYAyPQQrtPWC86leenpf/LII29Sz3Bvedhplrb0YDFBiuH0NQHQmJRenPlJeJ6/Lv/Y2Nj9Xfsf8TWnX1g==');
define('NONCE_SALT',       'kbWkcRmMPpAahj5tOHsv5YNzzKKXqrAFc5/am95vSEnR4OpLxh0eaH7WhMNHXzmoeM6OZcpOGo4Dokvi5Tet+Q==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
